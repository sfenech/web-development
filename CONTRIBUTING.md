# Contributing

Feel free to send Merge requests with fixes or create issues if you have comments or requests.

Note that this work is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/)
so any contributions will fall under the same license.