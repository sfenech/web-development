# Web Development

This is the course material for the [Web Development training](https://sfenech.gitlab.io/web-development/) for http://theshortcut.org/ school of startups 2017

Note that this is still a works in progress, and probably will remain so until the actual delivery.

## Synopsis

The aim of these two Web Development sessions is to remove as many barriers as possible from creating and serving web projects. As you might expect, one cannot expect to learn everything about web development in 4 hours but from my experience the hardest parts are

- Getting an end to end proof of concept working (after that it is easier to iterate and improve different parts)
- Knowing what are the relevant topics to search for (google is very handy… but first you need to know what to look for)

So I will be presenting the concepts, backed by working examples and links to reference material for those who want to learn more.

By the end of these 4 hours you should have

- A simple web page deployed and accessible
- Clear idea of what would be needed to build web applications and where to go to learn more.

I strongly believe in iteratively improving whatever you are doing, and that is the approach I will be taking during these 4 hours. So we will be starting with something simple, and then iteratively improving it. Even if we do not go through all the planned topics, I will be providing the slides so that you will know what are the next steps.

Some of the basic topics will be

- HTML & CSS
- Workflow Automation
- Version Control
- Deploying a website

The prerequisites are willingness to learn, experiment and fail. Of course, having prior knowledge of  HTML, CSS, Javascript… would be useful but not necessary. If we have people with very diverse experiences I would hope that experienced students will have some patience and also willingness to help out others with more limited experience.

## Tools Used

- [gulp](http://gulpjs.com/) for workflow automation. For full details of all gulp plugins used see [package.json](package.json)
- [nunjucks](https://mozilla.github.io/nunjucks/) for templating through [gulp-nunjucks-render](https://github.com/carlosl/gulp-nunjucks-render)
- [atom](https://atom.io/) as my text editor

### For presentation

- [reveal.js](https://github.com/hakimel/reveal.js/) for the main presentation
- [viz-lite.js](https://github.com/mdaines/viz.js) for the graphs written in graphviz.

### For backend application example

- [ninjaframework](http://www.ninjaframework.org/) as the main framework

## Folder Structure

- presentation/ : has the presentations
- pages/ : has pages, mainly related to the exercises
- templates/ : has the templates for all the pages
- lib/ : has third party libraries that are used, for example, highlight js
- js/ : has javascript that is used on the pages
- exercises/ : has the sources for the exercises
- exercises-completed/ : has the sources for the completed exercises
