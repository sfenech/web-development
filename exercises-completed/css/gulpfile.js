var gulp = require('gulp'),
  server = require('gulp-server-livereload'),
  sass = require('gulp-sass'),
  notifier = require('node-notifier')
  notify = require('gulp-notify'),
  gutil = require('gulp-util'),
  htmlmin = require('gulp-htmlmin');

gulp.task('pages',function(){
  return gulp.src(['*.html'])
  // minimise html
  .pipe(htmlmin({collapseWhitespace: true}))
  // output files in build folder
  .pipe(gulp.dest('build'))
});

gulp.task('pages-style',function(){
  return gulp.src(['*.scss','*.css'])
  .pipe(sass({outputStyle: 'compressed'}).on('error', function(error){
    var message = new gutil.PluginError('sass', error.messageFormatted).toString();
    notifier.notify({
      'title':'There is an error in the styles',
      'message':error.message
    });
    process.stderr.write(message + '\n');
    this.emit('end');
  }))
  .pipe(gulp.dest('build'));
});

gulp.task('build',['pages','pages-style']);

gulp.task('watch', function() {
  gulp.watch(['*.html'], ['pages']);
  gulp.watch(['*.scss','*.css'], ['pages-style']);
})

gulp.task('serve', ['build'], function() {
  gulp.src('build')
    .pipe(server({
      livereload: true,
      directoryListing: false,
      open: true
    }));
});

gulp.task('default',['build','serve','watch']);
