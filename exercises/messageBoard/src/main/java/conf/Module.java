package conf;

import com.google.inject.AbstractModule;
import com.google.inject.Provider;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.googlecode.objectify.Objectify;

import services.board.MessageService;

public class Module extends AbstractModule {

    @Override
    protected void configure() {
        bind(Objectify.class).toProvider(ObjectifyProvider.class);
    }

    @Provides
    @Singleton
    MessageService campaignService(Provider<Objectify> ofy) {
        return new MessageService(ofy);
    }

}
