package conf;

import javax.servlet.ServletContextEvent;

import com.google.appengine.api.utils.SystemProperty;

import ninja.utils.NinjaConstant;

public class NinjaServletListener extends ninja.servlet.NinjaServletListener {

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        if (SystemProperty.environment.value() == SystemProperty.Environment.Value.Development) {
            System.setProperty(NinjaConstant.MODE_KEY_NAME, NinjaConstant.MODE_DEV);
        }
        super.contextInitialized(servletContextEvent);
    }

}
