package conf;

import com.google.inject.Provider;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;

import services.board.Board;
import services.board.Message;

public class ObjectifyProvider implements Provider<Objectify> {
    @Override
    public Objectify get() {
        return ObjectifyService.ofy();
    }
    static {
        ObjectifyService.register(Board.class);
        ObjectifyService.register(Message.class);
    }
}
