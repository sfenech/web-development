package conf;

import controllers.Application;
import ninja.Router;
import ninja.application.ApplicationRoutes;

public class Routes implements ApplicationRoutes {

    @Override
    public void init(Router router) {
        router.GET().route("/").with(Application.class, "index");
        router.GET().route("/messages/{board: .*}").with(Application.class, "listMessages");
        router.POST().route("/messages/{board: .*}").with(Application.class, "createMessage");
        router.OPTIONS().route("/messages/.*").with(Application.class, "optionMessage");
    }

}
