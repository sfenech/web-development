package controllers;

import com.google.inject.Inject;

import ninja.Result;
import ninja.Results;
import ninja.params.PathParam;
import services.board.MessageService;

public class Application {

    private final MessageService messageService;
    @Inject
    public Application(MessageService messageService) {
        this.messageService = messageService;
    }
    public Result index() {
        return Results.ok().text().renderRaw("Hello from the message board! ".getBytes());
    }

    public Result listMessages(@PathParam("board") String board) {
        return ok().json()
                .render(messageService.getMessages(board));
    }

    public Result createMessage(@PathParam("board") String board, String message) {
        return ok().json()
                .render(messageService.createMessage(message, board));
    }
    public Result optionMessage(@PathParam("board") String board, String message) {
        return ok().json().render("");
    }

    private Result ok() {
        return Results.ok().addHeader("Access-Control-Allow-Origin", "*").addHeader("Access-Control-Allow-Headers",
                "Content-Type");
    }
}
