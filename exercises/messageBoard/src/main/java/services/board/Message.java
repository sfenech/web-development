package services.board;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Parent;

@Entity
public class Message {
    @Parent
    @JsonIgnore
    public Key<Board> board;
    @Id
    public Long id;
    public String message;
    public Long created;

}
