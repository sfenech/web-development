package services.board;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;

public class MessageService {

    private final Provider<Objectify> ofy;

    @Inject
    public MessageService(Provider<Objectify> ofy) {
        this.ofy = ofy;
    }

    public List<Message> getMessages(String board) {
        List<Message> messages=ofy.get().load().type(Message.class).ancestor(Key.create(Board.class, board)).list();
        Collections.sort(messages, new Comparator<Message>() {
            @Override
            public int compare(Message o1, Message o2) {
                if (o1.created == o2.created) {
                    return 0;
                } else {
                    return o2.created > o1.created ? -1 : 1;
                }
            }
        });
        return messages;
    }

    public Message createMessage(String textMessage, String board) {
        if (StringUtils.isNotEmpty(textMessage.trim())) {
            Message m = new Message();
            m.message = textMessage.trim();
            m.board = Key.create(Board.class, board);
            m.created = System.currentTimeMillis();
            Key<Message> key = ofy.get().save().entity(m).now();
            m.id = key.getId();
            return m;
        } else {
            return null;
        }
    }
}
