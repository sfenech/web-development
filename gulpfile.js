var gulp = require('gulp'),
  nunjucksRender = require('gulp-nunjucks-render'),
  server = require('gulp-server-livereload'),
  sass = require('gulp-sass'),
  notifier = require('node-notifier')
  notify = require('gulp-notify'),
  gutil = require('gulp-util'),
  fm = require('front-matter'),
  data = require('gulp-data'),
  htmlmin = require('gulp-htmlmin'),
  uglify = require('gulp-uglify'),
  viz = require("viz.js"),
  cheerio = require('cheerio'),
  through2 = require("through2"),
  concat = require('gulp-concat');

gulp.task('presentations',function(){
  return gulp.src('presentations/*.html')
  .pipe(data(function(file) {
      var content = fm(String(file.contents));
      file.contents = new Buffer(content.body);
      return content.attributes;
  }))
  // Renders template with nunjucks
  .pipe(nunjucksRender({
      path: ['presentations/templates','presentations/subsections','exercises','exercises-completed']
    }).on('error', function(error){
      var message = new gutil.PluginError('nunjucks', error,{showStack:true}).toString();
      notifier.notify({
        'title':error.name,
        'message':error.message
      });
      process.stderr.write(message + '\n');
      this.emit('end');
    }))
    // Render Dot diagrams using viz.js
    .pipe(through2.obj(function(file, encoding, callback) {
        const $ = cheerio.load(file.contents)
        $("[data-dot]").each(function(index,element){
          const $e=$(element);
          if($e.attr('data-dot-images')){
            $e.html(viz($e.text(),{images:$e.data('dot-images')}));
          }else{
            $e.html(viz($e.text()));
          }
          // Cleanup after viz, since it adds a listener on the process and forgets to remove it.
          process.removeListener('uncaughtException',process.listeners('uncaughtException')[process.listeners('uncaughtException').length-1]);
        });
        file.contents=new Buffer($.html());
        callback(null, file);
      }))
  // output files in build folder
  .pipe(gulp.dest('build/presentations'))
});

gulp.task('presentations-style',function(){
  return gulp.src('presentations/style.scss')
  .pipe(sass({outputStyle: 'compressed'}).on('error', function(error){
    var message = new gutil.PluginError('sass', error.messageFormatted).toString();
    notifier.notify({
      'title':'There is an error in the styles',
      'message':error.message
    });
    process.stderr.write(message + '\n');
    this.emit('end');
  }))
  .pipe(gulp.dest('build/presentations/'));
});

gulp.task('presentations-revealjs',function(){
  return gulp.src('presentations/revealjs/**/*')
  // output files in build folder
  .pipe(gulp.dest('build/presentations'))
});

gulp.task('presentations-js',function(){
  return gulp.src('presentations/js/*.js')
  .pipe(uglify())
  .pipe(concat("presentation.js"))
  .pipe(gulp.dest('build/presentations'))
});

gulp.task('presentations-js-lib',function(){
  return gulp.src('presentations/js/lib/*.js')
  .pipe(gulp.dest('build/presentations/js'))
});

gulp.task('presentations-imgs',function(){
  return gulp.src('presentations/imgs/**/*')
  // output files in build folder
  .pipe(gulp.dest('build/presentations/imgs'))
});

gulp.task('pages',function(){
  return gulp.src(['index.html','pages/*.html'])
  .pipe(data(function(file) {
      var content = fm(String(file.contents));
      file.contents = new Buffer(content.body);
      return content.attributes;
  }))
  // Renders template with nunjucks
  .pipe(nunjucksRender({
      path: ['templates','pages','exercises','exercises-completed']
    }))
  // minimise html
  .pipe(htmlmin({collapseWhitespace: true}))
  // output files in build folder
  .pipe(gulp.dest('build'))
});

gulp.task('pages-lib',function(){
  return gulp.src('lib/**/*')
  // output files in build folder
  .pipe(gulp.dest('build/lib'))
});

gulp.task('pages-imgs',function(){
  return gulp.src('pages/imgs/**/*')
  // output files in build folder
  .pipe(gulp.dest('build/imgs'))
});

gulp.task('pages-style',function(){
  return gulp.src('style.scss')
  .pipe(sass({outputStyle: 'compressed'}).on('error', function(error){
    var message = new gutil.PluginError('sass', error.messageFormatted).toString();
    notifier.notify({
      'title':'There is an error in the styles',
      'message':error.message
    });
    process.stderr.write(message + '\n');
    this.emit('end');
  }))
  .pipe(gulp.dest('build'));
});

gulp.task('pages-js',function(){
  return gulp.src('js/*.js')
  .pipe(uglify())
  .pipe(gulp.dest('build/js'))
});

gulp.task('exercises',function(){
  return gulp.src(['exercises-completed/**/*.html','!exercises-completed/**/node_modules/**','!exercises-completed/**/build/**'])
  .pipe(gulp.dest("build/exercises-completed"))
});

gulp.task('exercises-style',function(){
  return gulp.src(['exercises-completed/**/*.scss','exercises-completed/**/*.css','!exercises-completed/**/node_modules/**','!exercises-completed/**/build/**'])
  .pipe(sass({outputStyle: 'compressed'}).on('error', function(error){
    var message = new gutil.PluginError('sass', error.messageFormatted).toString();
    notifier.notify({
      'title':'There is an error in the styles',
      'message':error.message
    });
    process.stderr.write(message + '\n');
    this.emit('end');
  }))
  .pipe(gulp.dest('build/exercises-completed'));
});

gulp.task('build',['presentations','presentations-style','presentations-revealjs','presentations-js','presentations-js-lib','presentations-imgs','pages','pages-style','pages-lib','pages-js','pages-imgs','exercises','exercises-style']);

gulp.task('watch', function() {
  gulp.watch(['presentations/*.html','presentations/templates/**/*.html','presentations/subsections/**/*.html'], ['presentations']);
  gulp.watch(['presentations/style.scss'], ['presentations-style']);
  gulp.watch(['presentations/js/*.js'],['presentations-js']);
  gulp.watch(['presentations/imgs/**/*'],['presentations-imgs']);
  gulp.watch(['index.html','templates/**/*.html','pages/*.html'], ['pages']);
  gulp.watch(['style.scss'], ['pages-style']);
  gulp.watch(['pages/imgs/**/*'],['pages-imgs']);
  gulp.watch(['js/*.js'],['pages-js']);
  //gulp.watch(['!exercises-completed/*/node_modules/**','!exercises-completed/*/build/**','exercises-completed/**/*.scss','exercises-completed/**/*.css'], ['exercises-style']);
  //gulp.watch(['!exercises-completed/*/node_modules/**','!exercises-completed/*/build/**','exercises-completed/**/*.html'],['exercises']);
})

gulp.task('serve', ['build'], function() {
  gulp.src('build')
    .pipe(server({
      livereload: true,
      directoryListing: false,
      open: false
    }));
});

gulp.task('default',['build','serve','watch']);
