window.messageBoard=(function(){
  var serviceUrl="";
  var boardLocationCssSelector="";
  var boardId="";
  var messagesNode=document.createElement("div");
  messagesNode.className="messageBoardMessages";

  function addMessage(message){
    var dateNode=document.createElement("div");
    var messageDate=new Date(message.created);
    dateNode.className="messageDate";
    dateNode.innerText=messageDate
    var messageNode=document.createElement("div");
    messageNode.className="messageText"
    messageNode.innerText=message.message;
    messagesNode.appendChild(dateNode);
    messagesNode.appendChild(messageNode);
  }
  function createMessage(message){
    var req = new XMLHttpRequest();
    addMessage({
      "message":message,
      "created":new Date().getTime()
    });
    req.open("POST",serviceUrl+"/messages/"+boardId);
    req.setRequestHeader("Content-Type", "application/json");
    req.send(JSON.stringify(message));
  }
  function getMessages(){
    var req = new XMLHttpRequest();
    req.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
         var messages=JSON.parse(this.response);
         var messageBoardNode=document.querySelector(boardLocationCssSelector);
         for (var i = 0, len = messages.length; i < len; i++) {
           addMessage(messages[i]);
         }
         messageBoardNode.appendChild(messagesNode);

         var messageBoardForm=document.createElement("div");
         messageBoardForm.className="messageBoardForm";
         var textarea=document.createElement("textarea");
         messageBoardForm.appendChild(textarea);
         var commentButton=document.createElement("button");
         commentButton.innerText="Add Comment";
         commentButton.addEventListener("click",function(){
           if(textarea.value.trim().length>0){
             createMessage(textarea.value.trim());
             textarea.value="";
           }else{
             alert("Your comment cannot be empty!");
           }
         });
         messageBoardForm.appendChild(commentButton);
         messageBoardNode.appendChild(messageBoardForm);
      }
    };
    req.open("GET",serviceUrl+"/messages/"+boardId);
    req.send();
  }
  return {
    init: function(url,cssSelector){
      serviceUrl=url;
      boardLocationCssSelector=cssSelector;
      boardId=location.hostname+location.pathname;
      getMessages();
    }
  }
})();
