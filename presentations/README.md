# Quick Outline

## Pre-requisites
- Chrome
- Git
- Sourcetree
- Node
- Gulp
- Gitlab Account
- Google App Engine Account

## Session 1
- Client and Server
  - HTTP
  - DNS & IP
  - Ports
- The Browser
- Inspecting HTML
- HTML
- CSS
- Create simple website
- Intro to Git & Gitlab
  - Clone repository
- Intro to Workflow Automation with gulp
- Configure Gitlab Pipeline
- Deploy to gitlab.io

Advanced
- Static Blog
- Sass


## Session 2
- The Gitlab Pipeline
- Docker
- The Cloud (App Engine)
- Keys
- Rest
