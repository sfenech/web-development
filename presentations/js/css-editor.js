window.addEventListener("load", function (){
  var ccsExampleNodes=document.querySelectorAll("[data-css-example]");
  function initCss(cssExampleElement){
    var iframe=cssExampleElement.querySelector("iframe");
    var styleElement=iframe.contentDocument.createElement('style');
    var codeElement=cssExampleElement.querySelector("code");
    function update(){
      styleElement.innerHTML=codeElement.textContent;
    }
    codeElement.addEventListener("keyup", update, false);
    update();
    iframe.contentDocument.head.appendChild(styleElement);
    if(cssExampleElement.hasAttribute('data-html-example')){
      var buttons = cssExampleElement.querySelectorAll("li");
      var cssButton=buttons[0];
      var htmlButton=buttons[1];
      var htmlCodeElement=cssExampleElement.querySelectorAll("code")[1];

      htmlCodeElement.style.display="none";
      cssButton.className="selected";
      function updateHtml(){
        iframe.contentDocument.body.innerHTML=htmlCodeElement.textContent;
      }
      htmlCodeElement.addEventListener("keyup",updateHtml,false);
      updateHtml();

      cssButton.addEventListener("click", function (){
        cssButton.className="selected";
        htmlButton.className="";
        codeElement.style.display="block"
        htmlCodeElement.style.display="none";
      });
      htmlButton.addEventListener("click", function (){
        htmlButton.className="selected";
        cssButton.className="";
        htmlCodeElement.style.display="block";
        codeElement.style.display="none"
      });
    }
  }

  for (var i = 0, len = ccsExampleNodes.length; i < len; i++) {
    initCss(ccsExampleNodes[i]);
  };
  function initHtml(htmlExampleElement){
    if(!htmlExampleElement.hasAttribute('data-css-example')){
      var iframe=htmlExampleElement.querySelector("iframe");
      var htmlCodeElement=htmlExampleElement.querySelector("code");
      function updateHtml(){
        iframe.contentDocument.body.innerHTML=htmlCodeElement.textContent;
      }
      htmlCodeElement.addEventListener("keyup",updateHtml,false);
      updateHtml();
    }
  }
  var htmlExampleNodes=document.querySelectorAll("[data-html-example]");
  for (var i = 0, len = htmlExampleNodes.length; i < len; i++) {
    initHtml(htmlExampleNodes[i]);
  };
},false);
